import com.typesafe.config.ConfigFactory;
import org.junit.Test;
import services.DonneesCovidApp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class CsvFileWriterTest {

    @Test
    public void testCsvFileWriter() throws IOException {

        DonneesCovidApp.main(new String[0]);
        assertThat(
                Files.list(
                        Paths.get(ConfigFactory.load().getString("3il.path.output").replace("file://", ""))
                ).count()
        ).isGreaterThan(0L);
    }
}
