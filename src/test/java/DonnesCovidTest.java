import functions.reader.CsvFileReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class DonnesCovidTest {

    @Test
    public void testCsvFileReader() {
        CsvFileReader reader = new CsvFileReader("src/test/resources/data/input/covid-hosp-txad-age-fra-2022-01-11-19h09.csv");
        Dataset<Row> ds = reader.get();

        long actual = ds.count();
        int expected = 7392;

        assertThat(actual).isNotZero();
        assertThat(actual).isEqualTo(expected);
    }
}
