package services;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import functions.reader.CsvFileReader;
import functions.writer.CsvFileWriter;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

@Slf4j
public class DonneesCovidApp {
    public static void main( String[] args ) {
        Config config = ConfigFactory.load();

        String inputPathStr = config.getString("3il.path.input");
        String outputPathStr = config.getString("3il.path.output");


        SparkConf sparkConf = new SparkConf().setMaster("local[2]").setAppName("SparkApp");
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

        CsvFileReader reader = new CsvFileReader(inputPathStr, spark);

        Dataset<Row> ds = reader.get();

        CsvFileWriter writer = new CsvFileWriter(outputPathStr);

        writer.accept(ds);


    }
}
