package functions.reader;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.shaded.com.google.common.base.Supplier;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;


@Slf4j
@RequiredArgsConstructor
public class CsvFileReader implements Supplier<Dataset<Row>> {
    private final String inputPathStr;
    private final SparkSession sparkSession;

    public CsvFileReader(String inputPathStr) {
        this.inputPathStr = inputPathStr;
        this.sparkSession = SparkSession.builder().config(new SparkConf().setMaster("local[2]").setAppName("SparkApp")).getOrCreate();
    }

    @Override
    public Dataset<Row> get() {

        Dataset<Row> ds = sparkSession.read()
                .option("delimiter", ";")
                .option("header", "true")
                .csv(this.inputPathStr);

        return ds;
    }
}
