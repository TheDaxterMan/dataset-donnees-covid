package functions.writer;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import java.util.function.Consumer;


@Builder
@RequiredArgsConstructor
public class CsvFileWriter implements Consumer<Dataset<Row>> {
    private final String outputPathStr;

    @Override
    public void accept(Dataset<Row> ds) {
        ds.write().mode(SaveMode.Overwrite).parquet(outputPathStr);
    }
}
